# Server load prediction
This is a sample Keras project that can make predictions of server load time
series.

# Setup

```bash
$ pip install keras tensorflow pandas click matplotlib
$ ./launch.sh
```