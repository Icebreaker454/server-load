window_size=40
activations=('linear' 'sigmoid' 'tanh')
epochs=('100' '200' '400')

for activation in "${activations[@]}"
do
	for epoch in "${epochs[@]}"
	do
    		python main.py --window-size $window_size --activation $activation --epochs $epoch
	done
done
