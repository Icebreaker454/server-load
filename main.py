import random
import json

import click
import pandas as pd
import numpy as np
import tensorflow as tf
from matplotlib import pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler

from keras.models import Sequential
from keras.layers import LSTM, Dropout, Dense


def rmse(y_true, y_pred):
    return np.sqrt(np.mean((y_true - y_pred) ** 2))


RANDOM_SEED = 1337
TEST_SIZE = 0.20

random.seed(RANDOM_SEED)
np.random.seed(RANDOM_SEED)
scaler = MinMaxScaler()

data = pd.read_csv('normalized_data.csv', dtype={
                   'Value': np.float64}, sep=';', usecols=['Value'])
data = pd.DataFrame(scaler.fit_transform(data))


def prepare_windowed(series, window_size):
    series_copy = series.copy()
    for i in range(window_size):
        series = pd.concat([series,  series_copy.shift(-(i + 1))], axis=1)
    series.dropna(axis=0, inplace=True)
    return series.iloc[:, :-1], series.iloc[:, -1]


def prepare_model(window_size, activation):
    model = Sequential()

    activation_kwargs = {'activation': activation} if activation else {}

    model.add(LSTM(batch_input_shape=(None, window_size, 1),
                   output_dim=window_size, return_sequences=True))
    model.add(Dropout(0.5))
    model.add(LSTM(50))
    model.add(Dropout(0.2))
    model.add(Dense(1, **activation_kwargs))
    model.compile(loss='mse', optimizer='adam')
    model.summary()
    return model


@click.command()
@click.option('--epochs', default=100)
@click.option('--window-size', default=20)
@click.option('--activation', default='linear')
def main(epochs, window_size, activation):

    def custom_activation(x):
        return 0.6 * tf.math.tanh(x)

    plt.rcParams["figure.figsize"] = [16, 9]

    X, Y = prepare_windowed(data, window_size)
    trainX, testX, trainY, testY = train_test_split(
        X, Y, test_size=TEST_SIZE, random_state=RANDOM_SEED)
    trainX = np.reshape(trainX.values, (trainX.shape[0], trainX.shape[1], 1))
    testX = np.reshape(testX.values, (testX.shape[0], testX.shape[1], 1))
    model = prepare_model(window_size, activation if activation !=
                          'custom_tanh' else custom_activation)
    history = model.fit(
        trainX,
        trainY,
        verbose=True,
        epochs=epochs,
        validation_split=0.2
    )

    # History
    plt.xlabel('Epochs')
    plt.ylabel('Loss')
    plt.plot(history.history['loss'], label='Loss')
    plt.plot(history.history['val_loss'], label='Validation loss')
    plt.legend()
    plt.savefig(
        f'/home/icebreaker/Dropbox/Studies/Master/Diploma/Paper/Assets/{activation}_epochs_{epochs}_td_{window_size}_history.png',
        dpi=300)
    plt.close()

    # Prediction

    test_predict = model.predict(testX)

    testY = scaler.inverse_transform(testY.values.reshape(-1, 1))
    test_predict = scaler.inverse_transform(test_predict)

    plt.plot(range(len(testY)), testY, color='r', label='Test data')
    plt.plot(range(len(test_predict)), test_predict, color='b', label='Test prediction')
    plt.legend()
    plt.savefig(
        f'/home/icebreaker/Dropbox/Studies/Master/Diploma/Paper/Assets/{activation}_epochs_{epochs}_td_{window_size}_prediction.png',
        dpi=300
    )

    metadata = {
        'test_predict': test_predict[:, 0].tolist(),
        'test_data': testY[:, 0].tolist(),
        'rmse': rmse(test_predict[:, 0], testY[:, 0])
    }

    with open(f'/home/icebreaker/Dropbox/Studies/Master/Diploma/Paper/Assets/{activation}_epochs_{epochs}_td_{window_size}_metadata.json', 'w') as f:
        f.write(json.dumps(metadata))


if __name__ == '__main__':
    main()
